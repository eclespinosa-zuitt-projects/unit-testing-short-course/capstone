const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("forex_api_test_suite", (done) => {
  //add the solutions here
  const domain = "http://localhost:5001";

  it("POST /currency endpoint", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("POST /currency endpoint name missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint name not string", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: true,
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint name empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint exchange missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint exchange not an object", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: "lorem ipsum",
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint exchange empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {},
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint alias missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint alias not string", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: false,
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint alias empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint alias duplicated", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency endpoint complete and no duplicate", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: "Pound",
        name: "British Pound",
        ex: {
          peso: 70.2,
          usd: 1.3814,
          won: 1624.54,
          yuan: 8.84,
        },
      })
      .end((error, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
