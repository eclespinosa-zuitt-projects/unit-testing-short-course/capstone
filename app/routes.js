const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/rates", (req, res) => {
    return res.send({
      rates: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    //create if conditions here to check the currency
    const { alias, name, ex } = req.body;

    if (!name || typeof name !== "string" || name === "")
      return res.status(400).send({ Error: "Invalid input" });

    if (!alias || typeof alias !== "string" || alias === "")
      return res.status(400).send({ Error: "Invalid input" });

    if (!ex || typeof ex !== "object" || Object.entries(ex).length === 0)
      return res.status(400).send({ Error: "Invalid input" });

    if (exchangeRates[alias])
      return res.status(400).send({ Error: "Invalid input" });

    exchangeRates[alias] = { name, ex };

    //return status 200 if route is running
    return res.status(200).send({
      Message: "Route is running",
    });
  });
};
